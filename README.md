# MB90a_base_controller

MB90A Base controller code.
***Code runs only on Teensy 3.2 micro-controller board, use arduino Ide to burn (Teensyduino must be installed)***

## Sensors

- 4 [US-100 ultrasonics](https://media.digikey.com/pdf/Data%20Sheets/Adafruit%20PDFs/4019_Web.pdf)
- 5 [Sharp 2Y0A21F91 Irs](https://global.sharp/products/device/lineup/data/pdf/datasheet/gp2y0a21yk_e.pdf)
- 4 Encoders 1500 tick/rev 
- Voltage sensor 
    - Resistor 1: 6300 ohm
    - Resistor 2: 47000 ohm

## Motors

- 4 Motor dirvers [cytron](https://www.robotshop.com/media/files/PDF/user-manual-md10c-v2.pdf)
- 4 Dc Motors 
    - No load speed: 135RPM 
    - Rated speed: 105RPM
    - Wheel Diameter: 0.1524m


## ROS Topics
The topics published and subscribed by the base controller.

                    Sensors orientation 
                     __________________
                    |US2-IR2    US3-IR3|
                    |                  |
                M1  |E1              E2| M2
                    |        M         |
                    |        B         |
                    |US1     9      US4|
                    |IR1     0      IR4|
                    |        A         |
                    |                  |
                M3  |E3              E4| M4
                    |__________________|
                            IR5
- - - -
### Subscribed  

- Motors desired speed :
    - description : This topic recives required motors pwm values and direction, max pwm : ***255*** direction ***255/0***
    - name : `/motors_out`
    - msg_type : `mb90a_controller::Motors_output`
    - callback function : `motorsCallback`
- - - -
### Published 
       
- Voltage sensor data:
    - description : This topic publishes raw battery data in volts.
    - name : `/voltage_sensor`
    - msg_type : `std_msgs::Float32`
    - rate : `loop rate`
- - - -            
- Motors current speed data:
    - description : This topic publishes each motor speed in ***m/s***, calculation are made in a timer with 50hz frequency.
    - name : `/motor_speeds`
    - msg_type : `mb90a_controller::Encoders`
    - rate : `50 hz`
- - - -           
- Ultrasonic senosr left (US1) data:
    - description : This topic publishes ***Ultrasonic Left*** range data in ***meters*** and sensor info with the frame id.
    - name : `/ultrasonic_left`
    - msg_type : `sensor_msgs::Range`
    - rate : `loop rate`
- - - -                 
- Ultrasonic senosr front left (US2) data:
    - description : This topic publishes ***Ultrasonic Front Left*** range data in ***meters*** and sensor info with the frame id.
    - name : `/ultrasonic_front_left`
    - msg_type : `sensor_msgs::Range`
    - rate : `loop rate`
- - - -                 
- Ultrasonic senosr front right (US3) data:
    - description : This topic publishes ***Ultrasonic Front Right*** range data in ***meters*** and sensor info with the frame id.
    - name : `/ultrasonic_front_right`
    - msg_type : `sensor_msgs::Range`
    - rate : `loop rate`
- - - -                 
- Ultrasonic senosr right (US4) data:
    - description : This topic publishes ***Ultrasonic Right*** range data in ***meters*** and sensor info with the frame id.
    - name : `/ultrasonic_right`
    - msg_type : `sensor_msgs::Range`
    - rate : `loop rate`
- - - -                 
- Infrared senosr left (IR1) data:
    - description : This topic publishes ***Infrared Left*** range data in ***meters*** and sensor info with the frame id.
    - name : `/ir_left`
    - msg_type : `sensor_msgs::Range`
    - rate : `50 hz`
- - - -                 
- Infrared senosr front left (IR2) data:
    - description : This topic publishes ***Infrared Front Left*** range data in ***meters*** and sensor info with the frame id.
    - name : `/ir_front_left`
    - msg_type : `sensor_msgs::Range`
    - rate : `50 hz`
- - - -                
- Infrared senosr front right (IR3) data:
    - description : This topic publishes ***Infrared Front Right*** range data in ***meters*** and sensor info with the frame id.
    - name : `/ir_front_right`
    - msg_type : `sensor_msgs::Range`
    - rate : `50 hz`
- - - -                 
- Infrared senosr right (IR4) data:
    - description : This topic publishes ***Infrared Right range*** data and sensor info with the frame id.
    - name : `/ir_right`
    - msg_type : `sensor_msgs::Range`
    - rate : `50 hz`
- - - -                 
- Infrared senosr back (IR5) data:
    - description : This topic publishes ***Infrared Back range*** data and sensor info with the frame id.
    - name : `/ir_back`
    - msg_type : `sensor_msgs::Range`
    - rate : `50 hz`
- - - -       

