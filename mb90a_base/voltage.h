#ifndef VOLTAGE_H_
#define VOLTAGE_H_
#include "headers.h"
const float R1 = 6800; //value of resistor_1 on the voltage sensor
const float R2 = 47000; //value of resistor_2 on the voltage sensor

float voltage_readings(int voltage_pin);


#endif
