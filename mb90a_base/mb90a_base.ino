/*
 code -> mozo_bot teensy micro-controller code.
 Developed by -> omar walid.
 date -> 15/3/2021.
 MARSES, ROBOTICS SOLUTIONS.
 */
#include "headers.h"


#include "encoders.h"
#include "motors.h"
#include "voltage.h"
#include "ir.h"
#include "ultrasonics.h"
#include <TimerOne.h>
#include <TimerThree.h>
#include <std_msgs/Float64.h>

#include "Wire.h"

ros::NodeHandle  nh;

/*################################################*/
/*--------------PINs Decleration------------------*/
/*################################################*/

/*
        
         __________________
        |US2-IR2    US3-IR3|
        |                  |
    M1  |E1              E2| M2
        |                  |
        |                  |
        |US1            US4|
        |IR1            IR4|
        |                  |
        |                  |
    M3  |E3              E4| M4
        |__________________|
                IR5

*/




// ==========>>> Ultrasonics decleration available in ultrasonics.h 

//const int US1_trig = 26; // ultrasonic left
//const int US1_echo = 27; // ultrasonic left
//const int US2_trig = 28; // ultrasonic front left
//const int US2_echo = 29; // ultrasonic front left
//const int US3_trig = 30; // ultrasonic front right
//const int US3_echo = 31; // ultrasonic front right
//const int US4_trig = 24; // ultrasonic right
//const int US4_echo = 25; // ultrasonic right

//struct ultrasonics US1 = {US1_trig,US1_echo};
//struct ultrasonics US2 = {US2_trig,US2_echo};
//struct ultrasonics US3 = {US3_trig,US3_echo};
//struct ultrasonics US4 = {US4_trig,US4_echo};

const int IR_1 = A3; // IR left
const int IR_2 = A4; // IR front left
const int IR_3 = A5; // IR front right
const int IR_4 = A6; // IR right
const int IR_5 = A7; // IR Back

const int enc_1_A = 22; // Enc 1 Channel A
const int enc_1_B = 23; // Enc 1 Channel B
const int enc_2_A = 3; // Enc 2 Channel A
const int enc_2_B = 4; // Enc 2 Channel B
const int enc_3_A = 13; // Enc 3 Channel A
const int enc_3_B = 14; // Enc 3 Channel B
const int enc_4_A = 1; // Enc 4 Channel A
const int enc_4_B = 2; // Enc 4 Channel B


const int M_1_Dir = 12; // Motor 1 Direction
const int M_1_PWM = 10; // Motor 1 PWM
const int M_2_Dir = 11; // Motor 2 Direction
const int M_2_PWM = 9; // Motor 2 PWM
const int M_3_Dir =  7;// Motor 3 Direction
const int M_3_PWM =  5;// Motor 3 PWM
const int M_4_Dir =  8;// Motor 4 Direction
const int M_4_PWM =  6;// Motor 4 PWM


struct Motors Motor1 = {M_1_Dir,M_1_PWM};
struct Motors Motor2 = {M_2_Dir,M_2_PWM};
struct Motors Motor3 = {M_3_Dir,M_3_PWM};
struct Motors Motor4 = {M_4_Dir,M_4_PWM};


const int led_pin	= 32; 
const int voltage_pin = A2;

/*###############################################*/
std_msgs::Float32 voltage_msg;
sensor_msgs::Range US1_msg;
sensor_msgs::Range US2_msg;
sensor_msgs::Range US3_msg;
sensor_msgs::Range US4_msg;


//sensor_msgs::Range IR1_msg;
//sensor_msgs::Range IR2_msg;
//sensor_msgs::Range IR3_msg;
//sensor_msgs::Range IR4_msg;
//sensor_msgs::Range IR5_msg;
//mb90a_controller::Encoders motors_speed_msg;
/*##############################################*/

void msg_setup();
void led_robot_startup(int fade_vale);

/*******************ros subscribers callbacks**************************/
void motorsCallback(const mb90a_controller::Motors_output& msg)
{
  motor_drive(Motor1,msg.motor1_pwm,msg.motor1_dir);
  motor_drive(Motor2,msg.motor2_pwm,msg.motor2_dir);
  motor_drive(Motor3,msg.motor3_pwm,msg.motor3_dir);
  motor_drive(Motor4,msg.motor4_pwm,msg.motor4_dir);

}
/********************************************************************/


/***************************ros intializations**********************/
ros::Subscriber<mb90a_controller::Motors_output> motors_out("/motors_out", motorsCallback);
/*#####################################################################################*/
ros::Publisher voltage_sensor("/voltage_sensor", &voltage_msg  );

ros::Publisher ultra_left("/ultrasonic_left", &US1_msg);
ros::Publisher ultra_front_left("/ultrasonic_front_left", &US2_msg);
ros::Publisher ultra_front_right("/ultrasonic_front_right", &US3_msg);
ros::Publisher ultra_right("/ultrasonic_right", &US4_msg);

ros::Publisher ir_left("/ir_left", &IR1_msg);
ros::Publisher ir_front_left("/ir_front_left", &IR2_msg);
ros::Publisher ir_front_right("/ir_front_right", &IR3_msg);
ros::Publisher ir_right("/ir_right", &IR4_msg);
ros::Publisher ir_back("/ir_back", &IR5_msg);

ros::Publisher motor_speeds("/motor_speeds", &motors_speed_msg);
/*******************************************************************/



/*****************************setup*********************************/
void setup()
{
  /**********Pins_initalization***********/
  IR_init(IR_1,IR_2,IR_3,IR_4,IR_5); // IRs intitalization
  Encoders_init(enc_1_A,enc_1_B,enc_2_A,enc_2_B,enc_3_A,enc_3_B,enc_4_A,enc_4_B); // Encoders intitalization 
  // Motors intitalization
  Motor_init(Motor1);
  Motor_init(Motor2);
  Motor_init(Motor3);
  Motor_init(Motor4);
  // Ultrasonics
  Ultrasonic_init(US1);
  Ultrasonic_init(US2);
  Ultrasonic_init(US3);
  Ultrasonic_init(US4);
  pinMode(voltage_pin, INPUT); //volatge sensor
  pinMode(led_pin, OUTPUT); //led_pin



  /**********ROS_topics_setup***********/ 
  nh.initNode();

  nh.getHardware()->setBaud(500000);
  nh.subscribe(motors_out);
  
  nh.advertise(voltage_sensor);

  nh.advertise(ultra_left);
  nh.advertise(ultra_front_left);
  nh.advertise(ultra_front_right);
  nh.advertise(ultra_right);

  nh.advertise(ir_left);
  nh.advertise(ir_front_left);
  nh.advertise(ir_front_right);
  nh.advertise(ir_right);
  nh.advertise(ir_back);

  nh.advertise(motor_speeds);
  msg_setup();
  while(!nh.connected())
  {
    led_robot_startup(1000);
    nh.spinOnce();
  }  //wait for ROS connection

  Timer1.initialize(20000); // 0.02 seconds timmer  
  Timer1.attachInterrupt(myHandler2);

  pingTimer[0] = millis() + 75;
  for (uint8_t i = 1; i < SONAR_NUM; i++)
    pingTimer[i] = pingTimer[i - 1] + PING_INTERVAL;
}


int disconnected=0;
void loop()
{
    /// if serial gets disconnected /////
    while(!nh.connected())
   {
     noInterrupts();
     Timer1.stop();
     init_enc_param();
     motor_drive(Motor1,0,0);
     motor_drive(Motor2,0,0);
     motor_drive(Motor3,0,0);
     motor_drive(Motor4,0,0);
     delay (1);
     led_robot_startup(1000);
     nh.spinOnce(); 
     disconnected=1;
    }
    if (disconnected==1)
    {
      Timer1.restart();
      Timer1.start();
      interrupts();
      disconnected=0;
    }
    ////////////////////////////////////

    ping_s();
    US1_msg.range= (leftSensor/100);      
    US2_msg.range= (front_leftSensor/100);
    US3_msg.range= (front_rightSensor/100);
    US4_msg.range= (rightSensor/100);
    IR_distance(IR_1,IR_2,IR_3,IR_4,IR_5);  

    delay(50);
    nh.spinOnce(); 
}

void myHandler2()  // 0.02 seconds 
{
  RPM();
  if (fabs(mps1)>=0.02) // leds on if robot is moving
  {digitalWrite(led_pin,HIGH);}
  else{digitalWrite(led_pin,LOW);}

  motor_speeds.publish(&motors_speed_msg);

  voltage_msg.data=voltage_readings(voltage_pin);
  voltage_sensor.publish(&voltage_msg); // publish voltage sensor readings


  ir_left.publish(&IR1_msg);
  ir_front_left.publish(&IR2_msg);
  ir_front_right.publish(&IR3_msg);
  ir_right.publish(&IR4_msg);
  ir_back.publish(&IR5_msg);
   


  ultra_left.publish(&US1_msg);
  ultra_front_left.publish(&US2_msg);
  ultra_front_right.publish(&US3_msg);
  ultra_right.publish(&US4_msg);

}


void msg_setup()
{
  US1_msg.header.frame_id="us_left_link";
  US1_msg.radiation_type=sensor_msgs::Range::ULTRASOUND;
  US1_msg.min_range=0.02;
  US1_msg.max_range=4.0;
  US1_msg.field_of_view=0.261;

  US2_msg.header.frame_id="us_front_left_link";
  US2_msg.radiation_type=sensor_msgs::Range::ULTRASOUND;
  US2_msg.min_range=0.02;
  US2_msg.max_range=4.0;
  US2_msg.field_of_view=0.261;

  US3_msg.header.frame_id="us_front_right_link";
  US3_msg.radiation_type=sensor_msgs::Range::ULTRASOUND;
  US3_msg.min_range=0.02;
  US3_msg.max_range=4.0;
  US3_msg.field_of_view=0.261;

  US4_msg.header.frame_id="us_right_link";
  US4_msg.radiation_type=sensor_msgs::Range::ULTRASOUND;
  US4_msg.min_range=0.02;
  US4_msg.max_range=4.0;
  US4_msg.field_of_view=0.261;



  IR1_msg.header.frame_id="ir_left_link";
  IR1_msg.radiation_type=sensor_msgs::Range::INFRARED;
  IR1_msg.min_range=0.12;
  IR1_msg.max_range=0.7;
  IR1_msg.field_of_view=0.122173;

  IR2_msg.header.frame_id="ir_front_left_link";
  IR2_msg.radiation_type=sensor_msgs::Range::INFRARED;
  IR2_msg.min_range=0.12;
  IR2_msg.max_range=0.7;
  IR2_msg.field_of_view=0.122173;

  IR3_msg.header.frame_id="ir_front_right_link";
  IR3_msg.radiation_type=sensor_msgs::Range::INFRARED;
  IR3_msg.min_range=0.12;
  IR3_msg.max_range=0.7;
  IR3_msg.field_of_view=0.122173;

  IR4_msg.header.frame_id="ir_right_link";
  IR4_msg.radiation_type=sensor_msgs::Range::INFRARED;
  IR4_msg.min_range=0.12;
  IR4_msg.max_range=0.7;
  IR4_msg.field_of_view=0.122173;

  IR5_msg.header.frame_id="ir_back_link";
  IR5_msg.radiation_type=sensor_msgs::Range::INFRARED;
  IR5_msg.min_range=0.12;
  IR5_msg.max_range=0.7;
  IR5_msg.field_of_view=0.122173;
}


void led_robot_startup(int fade_vale)
{
  digitalWrite(led_pin,HIGH);
  delay(fade_vale);
  digitalWrite(led_pin,LOW);
  delay(fade_vale/2);
  digitalWrite(led_pin,HIGH);
  delay(fade_vale/4);
  digitalWrite(led_pin,LOW);
  delay(fade_vale/2);
}
