#include "ultrasonics.h"
#include <NewPing.h>

struct ultrasonics US1 = {US1_trig,US1_echo};
struct ultrasonics US2 = {US2_trig,US2_echo};
struct ultrasonics US3 = {US3_trig,US3_echo};
struct ultrasonics US4 = {US4_trig,US4_echo};

unsigned long pingTimer[SONAR_NUM]; 
unsigned int cm[SONAR_NUM];
uint8_t currentSensor=0; 

SimpleKalmanFilter KF_left(2, 2, 0.001);
SimpleKalmanFilter KF_front_left(2, 2, 0.001);
SimpleKalmanFilter KF_front_right(2, 2, 0.001);
SimpleKalmanFilter KF_right(2, 2, 0.001);
float oldSensorReading[4];    //Store last valid value of the sensors.


float leftSensor=0;
float front_leftSensor=0;
float front_rightSensor=0;
float rightSensor=0;

float leftSensorKalman=0;       //Store filtered sensor's value.
float front_leftSensorKalman=0;
float front_rightSensorKalman=0;
float rightSensorKalman=0;

unsigned long _timerStart = 0;

void Ultrasonic_init(struct ultrasonics us)
{
    pinMode(us.trig, OUTPUT); 
    pinMode(us.echo, INPUT); 
}

NewPing sonar[SONAR_NUM] = {
  NewPing(US1.trig, US1.echo, MAX_DISTANCE), // Trigger pin, echo pin, and max distance to ping.
  NewPing(US2.trig, US2.echo, MAX_DISTANCE),
  NewPing(US3.trig, US3.echo, MAX_DISTANCE),
  NewPing(US4.trig, US4.echo, MAX_DISTANCE)
};

NewPing sonar1(US1.trig, US1.echo, MAX_DISTANCE); 
NewPing sonar2(US2.trig, US2.echo, MAX_DISTANCE); 
NewPing sonar3(US3.trig, US3.echo, MAX_DISTANCE); 
NewPing sonar4(US4.trig, US4.echo, MAX_DISTANCE); 
void ping_s()
{
  leftSensor =           sonar1.ping_cm();
  front_leftSensor =     sonar2.ping_cm();
  front_rightSensor =    sonar3.ping_cm();
  rightSensor =          sonar4.ping_cm();
}

//// Functions to apply kalman filter
void sensorCycle() {
  for (uint8_t i = 0; i < SONAR_NUM; i++) {
    if (millis() >= pingTimer[i]) {
      pingTimer[i] += PING_INTERVAL * SONAR_NUM;
      if (i == 0 && currentSensor == SONAR_NUM - 1) oneSensorCycle();
      sonar[currentSensor].timer_stop();
      currentSensor = i;
      cm[currentSensor] = 0;
      sonar[currentSensor].ping_timer(echoCheck);
    }
  }
}

// If ping received, set the sensor distance to array.
void echoCheck() {
  if (sonar[currentSensor].check_timer())
    cm[currentSensor] = sonar[currentSensor].ping_result / US_ROUNDTRIP_CM;
}

//Return the last valid value from the sensor.
void oneSensorCycle() {
  leftSensor   = returnLastValidRead(0, cm[0]);
  front_leftSensor = returnLastValidRead(1, cm[1]);
  front_rightSensor  = returnLastValidRead(2, cm[2]);
  rightSensor = returnLastValidRead(3, cm[3]);
}

//If sensor value is 0, then return the last stored value different than 0.
int returnLastValidRead(uint8_t sensorArray, uint8_t cm) {
  if (cm != 0) {
    return oldSensorReading[sensorArray] = cm;
  } else {
    return oldSensorReading[sensorArray];
  }
}

//Apply Kalman Filter to sensor reading.
void applyKF() {
  leftSensorKalman = KF_left.updateEstimate(leftSensor);
  front_leftSensorKalman = KF_front_left.updateEstimate(front_leftSensor);
  front_rightSensorKalman = KF_front_right.updateEstimate(front_rightSensor);
  rightSensorKalman = KF_right.updateEstimate(rightSensor);

}

void startTimer() {
  _timerStart = millis();
}

bool isTimeForLoop(int _mSec) {
  return (millis() - _timerStart) > _mSec;
}





/////////// Default way of ping
float ultrasonic_calc(struct ultrasonics us)
{
   digitalWrite(us.trig, LOW);
   delayMicroseconds(2);
   // Sets the us.trig on HIGH state for 10 micro seconds
   digitalWrite(us.trig, HIGH);
   delayMicroseconds(10);
   digitalWrite(us.trig, LOW);
   // Reads the us.echo, returns the sound wave travel time in microseconds
   long duration = pulseIn(us.echo, HIGH);
   // Calculating the distance
   float distance= duration*0.034/2;
   return (distance/100);
}
