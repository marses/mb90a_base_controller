#ifndef ULTRASONICS_H_
#define ULTRASONICS_H_
#include "headers.h"
#include <NewPing.h>
#include <SimpleKalmanFilter.h>

const int US1_trig = 26; // ultrasonic left
const int US1_echo = 27; // ultrasonic left
const int US2_trig = 28; // ultrasonic front left
const int US2_echo = 29; // ultrasonic front left
const int US3_trig = 30; // ultrasonic front right
const int US3_echo = 31; // ultrasonic front right
const int US4_trig = 24; // ultrasonic right
const int US4_echo = 25; // ultrasonic right

#define SONAR_NUM 4          //The number of sensors. 
#define MAX_DISTANCE 400     //Mad distance to detect obstacles.
#define PING_INTERVAL 33     //Looping the pings after 33 microseconds.

extern unsigned long pingTimer[SONAR_NUM]; // Holds the times when the next ping should happen for each sensor.
extern unsigned int cm[SONAR_NUM];         // Where the ping distances are stored.
extern uint8_t currentSensor;         // Keeps track of which sensor is active.

extern unsigned long _timerStart;

extern int LOOPING;


extern float oldSensorReading[4];    //Store last valid value of the sensors.


extern float leftSensor;
extern float front_leftSensor;
extern float front_rightSensor;
extern float rightSensor;

extern float leftSensorKalman;       //Store filtered sensor's value.
extern float front_leftSensorKalman;
extern float front_rightSensorKalman;
extern float rightSensorKalman;


struct ultrasonics
{
    int trig;
    int echo;
};



extern struct ultrasonics US1;
extern struct ultrasonics US2;
extern struct ultrasonics US3;
extern struct ultrasonics US4;


void Ultrasonic_init(struct ultrasonics us);
float ultrasonic_calc(struct ultrasonics us);



// If ping received, set the sensor distance to array.
void echoCheck();
//Return the last valid value from the sensor.
void oneSensorCycle();
//If sensor value is 0, then return the last stored value different than 0.
int returnLastValidRead(uint8_t sensorArray, uint8_t cm);
//Apply Kalman Filter to sensor reading.
void applyKF();
void startTimer();
bool isTimeForLoop(int _mSec);
void sensorCycle();
void ping_s();
#endif
