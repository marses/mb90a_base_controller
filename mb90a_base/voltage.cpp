#include "voltage.h"

float filterFrequency_v = 0.5  ;
// create a one pole (RC) lowpass filter
FilterOnePole lowpassFilter_volt( LOWPASS, filterFrequency_v );

float voltage_readings(int voltage_pin)
{
    float value = lowpassFilter_volt.input( analogRead( voltage_pin ) );
    //float value = analogRead( voltage_pin );
    float v_out = (value/63231)*3.185; //3.185 is the output voltage of the sensor at full battery, 988 is the equivalent number of 3.185(where 1024 is equivalent to 3.3)
    float v_in  = v_out/(R1/(R1+R2)); //formula evaluate the the sensor input voltage which is the battery voltage  
    return v_in ;
}
