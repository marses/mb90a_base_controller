#include "motors.h"

void Motor_init(struct Motors motor)
{
  pinMode(motor.PWM,OUTPUT);
  pinMode(motor.Dir,OUTPUT);
  digitalWrite(motor.Dir,HIGH);
}

void motor_drive(struct Motors motor,double ref, int dir )
{
  digitalWrite(motor.Dir,dir);
  analogWrite(motor.PWM,ref);
}
