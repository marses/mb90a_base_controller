#ifndef MOTORS_H_
#define MOTORS_H_

#include "headers.h"
struct Motors
{
   int   Dir;
   int   PWM;
};


extern struct Motors Motor1;
extern struct Motors Motor2;
extern struct Motors Motor3;
extern struct Motors Motor4;

void Motor_init(struct Motors motor);
void motor_drive(struct Motors motor,double ref, int dir);


#endif
