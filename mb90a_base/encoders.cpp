#include "encoders.h"
#include <math.h>
#include <Arduino.h>

const int wheel_radius=0.076;
mb90a_controller::Encoders motors_speed_msg;

volatile int pos1=0,pos2=0,pos3=0,pos4=0;
volatile int enc1=0 ,enc2=0,enc3=0 ,enc4=0;
int p=0 ;
volatile double rpm1=0.0,rpm2=0.0,rpm3=0.0,rpm4=0.0;
volatile double mps1=0.0,mps2=0.0,mps3=0.0,mps4=0.0;
volatile int  counts1 =0 ,prev_counts1=0 ;
volatile int  counts2 =0 ,prev_counts2=0 ;
volatile int  counts3 =0 ,prev_counts3=0 ;
volatile int  counts4 =0 ,prev_counts4=0 ;

////////////////////////////encoders initalization///////////////////
void Encoders_init(int M1_chA,int M1_chB,int M2_chA,int M2_chB,int M3_chA,int M3_chB,int M4_chA,int M4_chB)
{

  delayMicroseconds(2000);
  pinMode(M1_chA, INPUT_PULLUP);
  pinMode(M1_chB, INPUT_PULLUP);
  pinMode(M2_chA, INPUT_PULLUP);
  pinMode(M2_chB, INPUT_PULLUP);
  pinMode(M3_chA, INPUT_PULLUP);
  pinMode(M3_chB, INPUT_PULLUP);
  pinMode(M4_chA, INPUT_PULLUP);
  pinMode(M4_chB, INPUT_PULLUP);

  delayMicroseconds(2000);
  attachInterrupt(M1_chA, M1isrPinAEn1, CHANGE); 
  attachInterrupt(M1_chB, M1isrPinBEn1, CHANGE); 
  attachInterrupt(M2_chA, M2isrPinAEn2, CHANGE); 
  attachInterrupt(M2_chB, M2isrPinBEn2, CHANGE); 
  attachInterrupt(M3_chA, M3isrPinAEn3, CHANGE); 
  attachInterrupt(M3_chB, M3isrPinBEn3, CHANGE); 
  attachInterrupt(M4_chA, M4isrPinAEn4, CHANGE); 
  attachInterrupt(M4_chB, M4isrPinBEn4, CHANGE); 

  delayMicroseconds(2000); 

}


//////////////////////Encoder counters///////////////////////////
void M1isrPinAEn1(){

  /* read pin B right away                                   */
  noInterrupts();
    int drB1 = digitalRead(enc_1_B);
  
  /* possibly wait before reading pin A, then read it        */
  // debounce(5);
    int  drA1 = digitalRead(enc_1_A);

  /* this updates the counter                                */
  if (drA1 == HIGH) {   /* low->high on A? */
      
    if (drB1 == LOW) {  /* check pin B */
    enc1++;
    pos1++;  /* going clockwise: increment         */
    } else {
    enc1--;
    pos1--;  /* going counterclockwise: decrement  */
    }
    
  } else {                       /* must be high to low on A */
  
    if (drB1 == HIGH) { /* check pin B */
    enc1++;
    pos1++;  /* going clockwise: increment         */
    } else {
    enc1--;
    pos1--;  /* going counterclockwise: decrement  */
    }
    
  } /* end counter update                                    */
interrupts();
} /* end ISR pin A Encoder 0                                 */

void M1isrPinBEn1(){ 
noInterrupts();
  /* read pin A right away                                   */
int  drA1 = digitalRead(enc_1_A);  
  /* possibly wait before reading pin B, then read it        */
  // debounce(5);
    int drB1 = digitalRead(enc_1_B);

  /* this updates the counter                                */
  if (drB1 == HIGH) {   /* low->high on B? */
  
    if (drA1 == HIGH) { /* check pin A */
    enc1++;
    pos1++;  /* going clockwise: increment         */
    } else {
        enc1--;
    pos1--;  /* going counterclockwise: decrement  */
    }
  
  } else {                       /* must be high to low on B */
  
    if (drA1 == LOW) {  /* check pin A */
    enc1++;
    pos1++;  /* going clockwise: increment         */
    } else {
        enc1--;
    pos1--;  /* going counterclockwise: decrement  */
    }
    
  } /* end counter update */
interrupts();
} /* end ISR pin B Encoder 0  */

//////////////////////////////////////////
void M2isrPinAEn2(){

  /* read pin B right away                                   */
  noInterrupts();
    int drB2 = digitalRead(enc_2_B);
  
  /* possibly wait before reading pin A, then read it        */
  // debounce(5);
    int  drA2 = digitalRead(enc_2_A);

  /* this updates the counter                                */
  if (drA2 == HIGH) {   /* low->high on A? */
      
    if (drB2 == LOW) {  /* check pin B */
    enc2++;
    pos2++;  /* going clockwise: increment         */
    } else {
    enc2--;
    pos2--;  /* going counterclockwise: decrement  */
    }
    
  } else {                       /* must be high to low on A */
  
    if (drB2 == HIGH) { /* check pin B */
    enc2++;
    pos2++;  /* going clockwise: increment         */
    } else {
    enc2--;
    pos2--;  /* going counterclockwise: decrement  */
    }
    
  } /* end counter update                                    */
interrupts();
} /* end ISR pin A Encoder 0                                 */

void M2isrPinBEn2(){ 
noInterrupts();
  /* read pin A right away                                   */
int  drA2 = digitalRead(enc_2_A);  
  /* possibly wait before reading pin B, then read it        */
  // debounce(5);
    int drB2 = digitalRead(enc_2_B);

  /* this updates the counter                                */
  if (drB2 == HIGH) {   /* low->high on B? */
  
    if (drA2 == HIGH) { /* check pin A */
    enc2++;
    pos2++;  /* going clockwise: increment         */
    } else {
        enc2--;
    pos2--;  /* going counterclockwise: decrement  */
    }
  
  } else {                       /* must be high to low on B */
  
    if (drA2 == LOW) {  /* check pin A */
    enc2++;
    pos2++;  /* going clockwise: increment         */
    } else {
        enc2--;
    pos2--;  /* going counterclockwise: decrement  */
    }
    
  } /* end counter update */
interrupts();
} /* end ISR pin B Encoder 0  */

//////////////////////////////////////////
void M3isrPinAEn3(){

  /* read pin B right away                                   */
  noInterrupts();
    int drB3 = digitalRead(enc_3_B);
  
  /* possibly wait before reading pin A, then read it        */
  // debounce(5);
    int  drA3 = digitalRead(enc_3_A);

  /* this updates the counter                                */
  if (drA3 == HIGH) {   /* low->high on A? */
      
    if (drB3 == LOW) {  /* check pin B */
    enc3--;
    pos3--;  /* going clockwise: increment         */
    } else {
        enc3++;
    pos3++;  /* going counterclockwise: decrement  */
    }
    
  } else {                       /* must be high to low on A */
  
    if (drB3 == HIGH) { /* check pin B */
    enc3--;
    pos3--;  /* going clockwise: increment         */
    } else {
    enc3++;
    pos3++;  /* going counterclockwise: decrement  */
    }
    
  } /* end counter update                                    */
interrupts();
} /* end ISR pin A Encoder 0                                 */

void M3isrPinBEn3(){ 
noInterrupts();
  /* read pin A right away                                   */
int  drA3 = digitalRead(enc_3_A);  
  /* possibly wait before reading pin B, then read it        */
  // debounce(5);
    int drB3 = digitalRead(enc_3_B);

  /* this updates the counter                                */
  if (drB3 == HIGH) {   /* low->high on B? */
  
    if (drA3 == HIGH) { /* check pin A */
    enc3--;
    pos3--;  /* going clockwise: increment         */
    } else {
        enc3++;
    pos3++;  /* going counterclockwise: decrement  */
    }
  
  } else {                       /* must be high to low on B */
  
    if (drA3 == LOW) {  /* check pin A */
    enc3--;
    pos3--;  /* going clockwise: increment         */
    } else {
        enc3++;
    pos3++;  /* going counterclockwise: decrement  */
    }
    
  } /* end counter update */
interrupts();
} /* end ISR pin B Encoder 0  */

//////////////////////////////////////////
void M4isrPinAEn4(){

  /* read pin B right away                                   */
  noInterrupts();
    int drB4 = digitalRead(enc_4_B);
  
  /* possibly wait before reading pin A, then read it        */
  // debounce(5);
    int  drA4 = digitalRead(enc_4_A);

  /* this updates the counter                                */
  if (drA4 == HIGH) {   /* low->high on A? */
      
    if (drB4 == LOW) {  /* check pin B */
    enc4--;
    pos4--;  /* going clockwise: increment         */
    } else {
        enc4++;
    pos4++;  /* going counterclockwise: decrement  */
    }
    
  } else {                       /* must be high to low on A */
  
    if (drB4 == HIGH) { /* check pin B */
    enc4--;
    pos4--;  /* going clockwise: increment         */
    } else {
    enc4++;
    pos4++;  /* going counterclockwise: decrement  */
    }
    
  } /* end counter update                                    */
interrupts();
} /* end ISR pin A Encoder 0                                 */

void M4isrPinBEn4(){ 
noInterrupts();
  /* read pin A right away                                   */
int  drA4 = digitalRead(enc_4_A);  
  /* possibly wait before reading pin B, then read it        */
  // debounce(5);
    int drB4 = digitalRead(enc_4_B);

  /* this updates the counter                                */
  if (drB4 == HIGH) {   /* low->high on B? */
  
    if (drA4 == HIGH) { /* check pin A */
    enc4--;
    pos4--;  /* going clockwise: increment         */
    } else {
        enc4++;
    pos4++;  /* going counterclockwise: decrement  */
    }
  
  } else {                       /* must be high to low on B */
  
    if (drA4 == LOW) {  /* check pin A */
    enc4--;
    pos4--;  /* going clockwise: increment         */
    } else {
        enc4++;
    pos4++;  /* going counterclockwise: decrement  */
    }
    
  } /* end counter update */
interrupts();
} /* end ISR pin B Encoder 0  */









////////////////////RPM/////////////////

void RPM()
{
    counts1=pos1-prev_counts1;
    counts2=pos2-prev_counts2;

    counts3=pos3-prev_counts3;
    counts4=pos4-prev_counts4;

    rpm1=counts1*0.5; // pulses * 1/timer * 1/(counts per rev)  * 60  
    rpm2=counts2*0.5; // pulses * 1/timer * 1/(counts per rev)  * 60   
    rpm3=counts3*0.5; // pulses * 1/timer * 1/(counts per rev)  * 60  
    rpm4=counts4*0.5; // pulses * 1/timer * 1/(counts per rev)  * 60   

    mps1=rpm1*0.00795;//wheel_radius*0.10472;  // wheel radius in meters * rpm * 0.1042
    mps2=rpm2*0.00795;//wheel_radius*0.10472;  // wheel radius in meters * rpm * 0.1042
    mps3=rpm3*0.00795;//wheel_radius*0.10472;  // wheel radius in meters * rpm * 0.1042
    mps4=rpm4*0.00795;//wheel_radius*0.10472;  // wheel radius in meters * rpm * 0.1042

    motors_speed_msg.enc1=mps1; // speed in m/s
    motors_speed_msg.enc2=mps2; // speed in m/s
    motors_speed_msg.enc3=mps3;
    motors_speed_msg.enc4=mps4;

    if (pos1>=0xFFFE)
    {pos1=0;}
    if (pos2>=0xFFFE)
    {pos2=0;}
    if (pos3>=0xFFFE)
    {pos3=0;}
    if (pos4>=0xFFFE)
    {pos4=0;}

    prev_counts1=pos1;
    prev_counts2=pos2;
    prev_counts3=pos3;
    prev_counts4=pos4;
}

void init_enc_param()
{
    pos1=0,pos2=0,pos3=0,pos4=0;
    enc1=0 ,enc2=0,enc3=0 ,enc4=0;
    rpm1=0.0,rpm2=0.0,rpm3=0.0,rpm4=0.0;
    mps1=0.0,mps2=0.0,mps3=0.0,mps4=0.0;
    counts1 =0 ,prev_counts1=0 ;
    counts2 =0 ,prev_counts2=0 ;
    counts3 =0 ,prev_counts3=0 ;
    counts4 =0 ,prev_counts4=0 ;
  }


void debounce(int del) {
  int k;
  for (k=0;k<del;k++) {
    k = k +0.0 +0.0 -0.0 +3.0 -3.0;
  }
}
