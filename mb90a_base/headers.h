#ifndef HEADERS_H_
#define HEADERS_H_

#include <Arduino.h>
#include <ros.h>
#include <ros/time.h>
#include <geometry_msgs/Twist.h>
#include <std_msgs/String.h>
#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <sensor_msgs/Range.h>
#include <mb90a_controller/Motors_output.h>
#include <mb90a_controller/Encoders.h>
#include <Filters.h>

extern ros::NodeHandle  nh;

extern sensor_msgs::Range US1_msg;
extern sensor_msgs::Range US2_msg;
extern sensor_msgs::Range US3_msg;
extern sensor_msgs::Range US4_msg;

extern sensor_msgs::Range IR1_msg;
extern sensor_msgs::Range IR2_msg;
extern sensor_msgs::Range IR3_msg;
extern sensor_msgs::Range IR4_msg;
extern sensor_msgs::Range IR5_msg;

extern mb90a_controller::Encoders motors_speed_msg;




#endif
