#ifndef ENCODERS_H_
#define ENCODERS_H_
#include "headers.h"



extern volatile int pos1,pos2,pos3,pos4;
extern volatile int enc1 ,enc2,enc3 ,enc4;
extern int p ;
extern volatile double rpm1,rpm2,rpm3,rpm4;
extern volatile double mps1,mps2,mps3,mps4;
extern volatile int  counts1  ,prev_counts1 ;
extern volatile int  counts2  ,prev_counts2 ;
extern volatile int  counts3  ,prev_counts3 ;
extern volatile int  counts4  ,prev_counts4 ;


extern const int enc_1_A;
extern const int enc_1_B;
extern const int enc_2_A;
extern const int enc_2_B;
extern const int enc_3_A;
extern const int enc_3_B;
extern const int enc_4_A;
extern const int enc_4_B;

// extern mb90a_controller::Encoders motors_speed_msg;

void debounce(int del);
void Encoders_init(int M1_chA,int M1_chB,int M2_chA,int M2_chB,int M3_chA,int M3_chB,int M4_chA,int M4_chB);
void M1isrPinAEn1();
void M1isrPinBEn1();
void M2isrPinAEn2();
void M2isrPinBEn2();
void M3isrPinAEn3();
void M3isrPinBEn3();
void M4isrPinAEn4();
void M4isrPinBEn4();

void init_enc_param();
void RPM();


#endif
