#ifndef IR_H_
#define IR_H_
#include "headers.h"


extern float distance1 ;
extern float distance2 ;
extern float distance3 ;
extern float distance4 ;
extern float distance5 ;

extern const int IR_1; // IR left
extern const int IR_2; // IR front left
extern const int IR_3; // IR front right
extern const int IR_4; // IR right
extern const int IR_5; // IR Back

void IR_init(int IR_1,int IR_2,int IR_3,int IR_4,int IR_5);
void IR_distance(int IR_1,int IR_2,int IR_3,int IR_4,int IR_5);

#endif
