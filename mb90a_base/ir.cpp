#include "ir.h"


float distance1=0 ;
float distance2=0 ;
float distance3=0 ;
float distance4=0 ;
float distance5=0 ;


float filterFrequency = 0.6;
// create a one pole (RC) lowpass filter
FilterOnePole lowpassFilter1( LOWPASS, filterFrequency );
FilterOnePole lowpassFilter2( LOWPASS, filterFrequency );
FilterOnePole lowpassFilter3( LOWPASS, filterFrequency );
FilterOnePole lowpassFilter4( LOWPASS, filterFrequency );
FilterOnePole lowpassFilter5( LOWPASS, filterFrequency );


sensor_msgs::Range IR1_msg;
sensor_msgs::Range IR2_msg;
sensor_msgs::Range IR3_msg;
sensor_msgs::Range IR4_msg;
sensor_msgs::Range IR5_msg;

void IR_init(int IR_1,int IR_2,int IR_3,int IR_4,int IR_5)
{
  pinMode(IR_1,INPUT);
  pinMode(IR_2,INPUT);
  pinMode(IR_3,INPUT);
  pinMode(IR_4,INPUT);
  pinMode(IR_5,INPUT);
  analogReadResolution(16);
  analogReadAveraging(32);
}
void IR_distance(int IR_1,int IR_2,int IR_3,int IR_4,int IR_5)
{

//  float read1= lowpassFilter1.input( analogRead(IR_1)) ;
//  float read2= lowpassFilter2.input( analogRead(IR_2)) ;
//  float read3= lowpassFilter3.input( analogRead(IR_3)) ;
//  float read4= lowpassFilter4.input( analogRead(IR_4)) ;
//  float read5= lowpassFilter5.input( analogRead(IR_5)) ;
  float read1= analogRead(IR_1) ;
  float read2= analogRead(IR_2) ;
  float read3= analogRead(IR_3) ;
  float read4= analogRead(IR_4) ;
  float read5= analogRead(IR_5) ;

  if (read1<8960)
  {distance1=0;}
  else{
//  distance1=abs((9760/(read1-17))-2);
  distance1=29.988 * pow(read1*(3.2/65535),-1.173);} 
    
  if (read2<8960)
  {distance2=0;}
  else{
//  distance2=abs((9760/(read2-17))-2);
  distance2=29.988 * pow(read2*(3.3/65535),-1.173);}

  if (read3<8960)
  {distance3=0;}
  else{
//  distance3=abs((9760/(read3-17))-2);
  distance3=29.988 * pow(read3*(3.2/65535),-1.173);}

  if (read4<8960)
  {distance4=0;}
  else{
//  distance4=abs((9760/(read4-17))-2);
  distance4=29.988 * pow(read4*(3.2/65535),-1.173);} 

  if (read5<8960)
  {distance5=0;}
  else{
//  distance5=abs((9760/(read5-17))-2);
  distance5=29.988 * pow(read5*(3.2/65535),-1.173);}  


  IR1_msg.range=distance1/100;
  IR2_msg.range=distance2/100;
  IR3_msg.range=distance3/100;
  IR4_msg.range=distance4/100;
  IR5_msg.range=distance5/100;
}
